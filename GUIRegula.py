from tkinter import *

from GUIBiseccion import GUIBiseccion

class Regula(GUIBiseccion):
    def __init__(self,pol,precision,root,Mensaje,tbPol,stringpol):
        GUIBiseccion.__init__(self,pol,precision,root,Mensaje,tbPol,stringpol)
        self.Mensaje.set("Mostrando la gráfica del polinomio. Pulsar los botones de iteración adelante o "
                                     "atrás para ver la resolución paso a paso por el método de Regula Falsi "
                                     ", o el botón \"Resolver\" para"
                                     " obtener las raices del polinomio de manera directa.")
        self.lbVm.configure(text = "Intersección secante y eje de abcisas")
        # self.lbMensajes.configure(font = ("Dejavu",15))
        self.lbVm.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5 - self.lbVm.winfo_width()) / 13, y=self.alto/2.48)

    def ObtenerDerivada(self):
        """ Llenar la lista de valores de los extremos de los intervalos y de los valores del polinomio en el punto de
            intersección de la secante con el eje de abcisas"""
        # Ocultar la etiqueta del rotulo y la caja de texto de la derivada
        self.lbDerivada.place_forget()
        self.lbRotuloDerivada.place_forget()
        raiz = self.pol.ObtenerRaizFalsi(self.extremos,self.valoresm,self.precision)
        if raiz is None:
            self.Finalizar()

    def PintarIntervalo(self):
        """ Dibuja el intervalo en funcion de la posicion del puntero """

        if self.directa:
            return
        if self.puntero < 0:
            self.puntero = 0
        factory = self.altografico / self.anchografico * self.factorx
        # Obtener los valores de los extremos y el punto de interseccion de la secante con el eje de abcisas
        # del intervalo y los valores del polinomio en esos puntos
        try:
            a = self.extremos[self.puntero]
            b = self.extremos[self.puntero + 1]
        except IndexError:
            return
        fa = self.pol.EvaluarPolinomio(a)
        fb = self.pol.EvaluarPolinomio(b)
        pi = self.pol.InterseccionSecante(a,fa,b,fb)
        fpi = self.pol.EvaluarPolinomio(pi)
        # Mostrar los valores en las etiquetas del lado izquierdo
        valorizquierdo = "x: " + str(round(a, 3)) + "     fx: " + str(round(fa, 3))
        valorderecho = "x: " + str(round(b, 3)) + "     fx: " + str(round(fb, 3))
        puntointerseccion = "x: " + str(round(pi, 3)) + "     fx: " + str(round(fpi, 3))
        self.stringvalorx.set(valorizquierdo)
        self.stringvalory.set(valorderecho)
        self.stringvalorpendiente.set(puntointerseccion)

        if self.puntero == 0:
            lectura = "Comenzamos estableciendo un intervalo entre dos puntos, punto de color rojo claro y " \
                      "de color naranja teniendo en cuenta que" \
                      " el valor del polinomio en los extremos del intervalo tenga signo distinto." \
                      "Así aseguramos la existencia de un punto dentro del intervalo donde el valor" \
                      " del polinomio sea cero.\nEn cada iteración se calcula el punto de intersección de la linea entre" \
                      " los valores del polinomio en los extremos ( linea de color azul ) y el eje de abcisas." \
                      " Cuando el valor del polinomio en el punto de intersección de la linea secante sea" \
                      " menor que la precisión introducida, se habrá obtenido una raiz del polinomio."
        else:
            lectura = ""
        """
        if self.puntero == len(self.extremos) - 2:
            self.raices.append(pi)
            self.MostrarRaiz()
            self.extremos.clear()
            return
        elif fa * fpi < 0:
        """
        if fa * fpi < 0:
            lectura += "\nComo el valor del polinomio en el extremo izquierdo multiplicado por el valor" \
                       "del polinomio en el punto de intersección de la secante con el eje de abcisas" \
                       " es negativo, el extremo izquierdo se mantendrá" \
                       " en la siguiente iteración. El punto de intersección de la secante con el eje de" \
                       " abcisas pasará a ser el extremo derecho."
        elif fb * fpi < 0:
            lectura += "\nComo el valor del polinomio en el extremo derecho multiplicado por el valor " \
                       "del polinomio en el punto de intersección de la secante con el eje de abcisas" \
                       " es negativo, el extremo derecho se mantendrá " \
                       "en la siguiente iteración. El punto de intersección de la secante con el eje de" \
                       " abcisas pasará a ser el extremo izquierdo."
        # Dibujar el extremo izquierdo
        self.daGrafico.create_oval(self.anchografico / 2 + a * self.factorx + self.desplx - 5,
                                   self.altografico / 2 + self.desply - 5,
                                   self.anchografico / 2 + a * self.factorx + self.desplx + 5,
                                   self.altografico / 2 + self.desply + 5, fill="Coral")
        # Dibujar el valor del polinomio en el extremo izquierdo
        self.daGrafico.create_oval(self.anchografico / 2 + a * self.factorx + self.desplx - 5,
                                   self.altografico / 2 - fa * factory + self.desply - 5,
                                   self.anchografico / 2 + a * self.factorx + self.desplx + 5,
                                   self.altografico / 2 - fa * factory + self.desply + 5,
                                   fill="Coral")
        # Dibujar la linea entre el extremo izquierdo y el valor del polinomio en ese punto
        self.daGrafico.create_line(self.anchografico / 2 + a * self.factorx + self.desplx,
                                   self.altografico / 2 + self.desply,
                                   self.anchografico / 2 + a * self.factorx + self.desplx,
                                   self.altografico / 2 - fa * factory + self.desply,
                                   fill="Coral")
        # Dibujar el extremo derecho
        self.daGrafico.create_oval(self.anchografico / 2 + b * self.factorx + self.desplx - 5,
                                   self.altografico / 2 + self.desply - 5,
                                   self.anchografico / 2 + b * self.factorx + self.desplx + 5,
                                   self.altografico / 2 + self.desply + 5,
                                   fill="Orange")
        # Dibujar el valor del polinomio en el extremo derecho
        self.daGrafico.create_oval(self.anchografico / 2 + b * self.factorx + self.desplx - 5,
                                   self.altografico / 2 - fb * factory + self.desply - 5,
                                   self.anchografico / 2 + b * self.factorx + self.desplx + 5,
                                   self.altografico / 2 - fb * factory + self.desply + 5,
                                   fill="Orange")
        # Dibujar la linea entre el extremo derecho y el valor del polinomio en ese punto
        self.daGrafico.create_line(self.anchografico / 2 + b * self.factorx + self.desplx,
                                   self.altografico / 2 + self.desply,
                                   self.anchografico / 2 + b * self.factorx + self.desplx,
                                   self.altografico / 2 - fb * factory + self.desply,
                                   fill="Orange")
        # Dibujar el punto de interseccion de la secante con el eje de abcisas
        self.daGrafico.create_oval(self.anchografico / 2 + pi * self.factorx + self.desplx - 5,
                                   self.altografico / 2 + self.desply - 5,
                                   self.anchografico / 2 + pi * self.factorx + self.desplx + 5,
                                   self.altografico / 2 + self.desply + 5,
                                   fill="Chartreuse")
        # Dibujar valor del polinomio en el punto de interseccion de la secante con el eje de abcisas
        self.daGrafico.create_oval(self.anchografico / 2 + pi * self.factorx + self.desplx - 5,
                                   self.altografico / 2 - fpi * self.factory + self.desply - 5,
                                   self.anchografico / 2 + pi * self.factorx + self.desplx + 5,
                                   self.altografico / 2 - fpi * factory + self.desply + 5,
                                   fill="Chartreuse")
        # Dibujar la linea entre el punto medio y el valor del polinomio en ese punto
        self.daGrafico.create_line(self.anchografico / 2 + pi * self.factorx + self.desplx,
                                   self.altografico / 2 + self.desply,
                                   self.anchografico / 2 + pi * self.factorx + self.desplx,
                                   self.altografico / 2 - fpi * factory + self.desply,
                                   fill="Chartreuse")
        # Dibujar la linea secante entre los valores del polinomio en los extremos
        self.daGrafico.create_line(self.anchografico/2 + a * self.factorx+self.desplx,
                                   self.altografico/2 - fa * factory+self.desply,
                                   self.anchografico/2 + b * self.factorx+self.desplx,
                                   self.altografico/2 - fb * factory+self.desply,
                                   fill = "Cyan")
        # Mostrar la lectura en la etiqueta de los mensajes
        self.Mensaje.set(lectura)

        if self.puntero == len(self.extremos) - 2:
            self.raices.append(pi)
            self.MostrarRaiz()
            self.extremos.clear()
            if self.puntero == 0:
                lecturaactual = self.Mensaje.get()
                self.Mensaje.set("Comenzamos estableciendo un intervalo entre dos puntos, punto de color rojo claro y " \
                      "de color naranja teniendo en cuenta que" \
                      " el valor del polinomio en los extremos del intervalo tenga signo distinto." \
                      "Así aseguramos la existencia de un punto dentro del intervalo donde el valor" \
                      " del polinomio sea cero.\nEn cada iteración se calcula el punto de intersección de la linea entre" \
                      " los valores del polinomio en los extremos ( linea de color azul ) y el eje de abcisas." \
                      " En este caso, el valor del polinomio en el punto de intersección de la linea secante es" \
                      " menor que la precisión introducida.\n " + lecturaactual)