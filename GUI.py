# !/usr/bin/python3.7
# -*- coding:utf-8 -*-


from tkinter import *
from tkinter import ttk


import Polinomio
import sys
import os

from Polinomio import Polinomio
from Polinomio import PolinomioCopia

import math
from math import log, ceil


class GUI():
    # Atributos de la clase
    incremento = 0.1  # incremento de la X en cada iteracion

    def __init__(self, pol, precision, toplevel, Mensaje, tbPol, tipo,stringpol):
        self.toplevel = toplevel
        self.tipo = tipo
        self.Mensaje = Mensaje
        self.stringpol = stringpol
        # Atributos del objeto
        self.pol = pol  # Objeto Polinomio para realizar los calculos
        self.precision = precision  # Precision con que se calcularan las raices
        self.polmemo = pol  # Objeto polinomio original para recordar cuando se vaya reduciendo el polinomio introducido
        self.derivada = Polinomio([])  # Objeto Polinomio que sera la derivada del polinomio
        self.valoresx = {}  # Diccionario para guardar los valores de x e y de cada iteracion
        self.valoresm = []  # Lista para guardar los valores de la pendiente en cada iteracion
        self.raices = []  # Para guardar las raices
        self.puntero = -1  # Indice del elemento clave/valor del diccionario valorex y de la lista m con los que se está operando
        self.pd = False;  # Si es true, indica al metodo on_draw que pinte la derivada
        self.desplx = 0  # Desplazamiento en x desde el origen de coordenadas
        self.desply = 0  # Desplazamiento en y desde el origen de coordenadas
        self.escala = 1  # Factor de escala de visualizacion
        self.tangente = False  # Será True cuando se dibuje la primera linea tangente
        self.directa = False
        self.alto,self.ancho = self.toplevel.winfo_screenheight(), self.toplevel.winfo_screenwidth() # Obtener el ancho y alto de la pantalla
        # DrawingArea para el grafico
        self.daGrafico = Canvas(self.toplevel,background = "black")
        self.anchografico, self.altografico = self.ancho - self.ancho/5, self.alto-self.alto/2.5
        self.daGrafico.place(x = 10, y = self.alto/4.2,width = self.anchografico, height = self.altografico)
        self.factorx = 60  # Pixeles por unidad del valor de la X
        self.factory = self.altografico * self.factorx / self.anchografico
        # Pintar los ejes de coordenadas , las divisiones y el grafico
        self.Redibujar()
        # Etiqueta para el rotulo del valor de x
        letrarotulos = "Dejavu 13 underline"
        letravalores = "Dejavu 13 bold"
        self.lbVx = Label(self.toplevel,background = "seagreen",text = "Valor de X",font = letrarotulos)
        self.lbVx.place(x = (self.ancho - self.ancho/5) + (self.ancho/5 - self.lbVx.winfo_width())/3, y = self.alto/4.2)
        # Etiqueta para el valor de x
        fondocajas = "lightgreen"
        self.stringvalorx = StringVar()
        self.lbX = Label(self.toplevel,textvariable = self.stringvalorx,font = letravalores,background = fondocajas,
                         fg = "Black")
        self.lbX.place(x = (self.ancho - self.ancho/5) + 20, y = self.alto/4.2+30,width = self.ancho/5-40)
        # Etiqueta para el rotulo de Y
        self.lbVY = Label(self.toplevel, background="seagreen", text="Valor de Y", font=letrarotulos)
        self.lbVY.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5 - self.lbVY.winfo_width()) / 3,
                        y=self.alto/3.15)
        # Etiqueta para el valor de Y
        self.stringvalory = StringVar()
        clave = self.valoresx.get(self.puntero)
        if clave is not None:
            self.stringvalory.set = str(self.valoresx[clave])
        self.lbY = Label(self.toplevel,textvariable = self.stringvalory,font = letravalores,background = fondocajas,
                         fg = "Black")
        self.lbY.place(x = (self.ancho - self.ancho/5) + 20, y = self.alto/3.15+30,width = self.ancho/5-40)
        # Etiqueta para el rotulo del valor de la pendiente
        self.lbVm = Label(self.toplevel, background="seagreen", text="Valor de la pendiente", font=letrarotulos)
        self.lbVm.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5 - self.lbVm.winfo_width()) / 5,
                        y=self.alto/2.52)
        # Etiqueta para el valor de la pendiente
        self.stringvalorpendiente = StringVar()
        if len(self.valoresm) > 0:
            self.stringvalorpendiente.set(self.valoresm[self.puntero])
        self.lbM = Label(self.toplevel, textvariable=self.stringvalorpendiente, font=letravalores,background = fondocajas,
                         fg = "Black")
        self.lbM.place(x=(self.ancho - self.ancho / 5) + 20, y=self.alto/2.52+30, width=self.ancho / 5 - 40)

        # Etiqueta para el rotulo del nuevo valor de x
        self.lbVnuevax = Label(self.toplevel, background="seagreen", text="Siguiente valor de X", font=letrarotulos)
        self.lbVnuevax.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5 - self.lbVm.winfo_width()) / 4,
                             y=self.alto/2.1)
        # Etiqueta para el nuevo valor de X
        self.stringnuevax = StringVar()
        if len(self.valoresm) > 0:
            self.stringnuevax.set(str(self.valoresx.get(self.puntero+1)))
        self.lbNuevax = Label(self.toplevel, textvariable=self.stringnuevax, font=letravalores,background = fondocajas,
                              fg = "Black")
        self.lbNuevax.place(x=(self.ancho - self.ancho / 5) + 20, y=self.alto/2.1+30, width=self.ancho / 5 - 40)

        # Etiqueta para el rotulo de los botones de zoom
        self.lbZoom = Label(self.toplevel,background = "seagreen",text = "Zoom y desplazamiento",font = letrarotulos)
        self.lbZoom.place(x = (self.ancho - self.ancho/5) + (self.ancho/5 - self.lbVx.winfo_width())/7,
                          y = self.alto/1.8)
        # Botones de zoom
        ladobotones = 40
        colorbotones = "cornflowerblue"
        self.btZoomMenos = Button(background = colorbotones,text = "-",font = "Dejavu 17 bold",command = self.on_btZoomMenos_clicked)
        self.btZoomMenos.place(x = (self.ancho-self.ancho/5)+(self.ancho/5-100)/2,y = self.alto/1.8+ladobotones,width = ladobotones,height = ladobotones)
        self.btZoomMas = Button(background = colorbotones,text = "+",font = "Dejavu 17 bold",command = self.on_btZoomMas_clicked)
        self.btZoomMas.place(x = (self.ancho-self.ancho/5)+(self.ancho/5)/1.8,y = self.alto/1.8+ladobotones,width = ladobotones,height = ladobotones)

        # Botones de desplazamiento
        self.btArriba = Button(background = "cyan",text = "\u25B4",font = "Arial 29 bold",
                               command = self.on_btArriba_clicked,fg = "DarkBlue")
        self.btArriba.place(x = (self.ancho-self.ancho/5)+(self.ancho/5)/2.3,y = self.alto/1.45,height = ladobotones,width = ladobotones)
        self.btAbajo = Button(background="coral", text="\u25BE", font="Arial 29 bold",
                               command=self.on_btAbajo_clicked, fg="red")
        self.btAbajo.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5) / 2.3, y = self.alto/1.45+ladobotones*2, height=ladobotones,
                            width=ladobotones)
        self.btIzquierda = Button(background="DarkMagenta", text="\u25C2", font="Arial 29 bold",
                              command=self.on_btIzquierda_clicked, fg="DarkOrchid")
        self.btIzquierda.place(x=(self.ancho - self.ancho / 5) + (self.ancho/5)/2.3-ladobotones, y=self.alto/1.45+ladobotones, height=ladobotones,
                           width=ladobotones)
        self.btDerecha = Button(background="Blue", text="\u25B8", font="Arial 29 bold",
                              command=self.on_btDerecha_clicked, fg="Cyan")
        self.btDerecha.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5) /2.3+ladobotones, y=self.alto/1.45+ladobotones, height=ladobotones,
                           width=ladobotones)
        self.btHome = Button(background="Gray", text="\u2302", font="Arial 31 bold",
                                command=self.on_btHome_clicked, fg="Black")
        self.btHome.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5) / 2.3, y=self.alto/1.45+ladobotones, height=ladobotones,
                              width=ladobotones)
        # Etiqueta del rotulo de la derivada
        self.lbRotuloDerivada = Label(self.toplevel, text="Derivada del polinomio: ", font=letrarotulos,
                                      background = "seagreen")
        self.lbRotuloDerivada.place(x=5, y = self.alto/1.18)
        # Etiqueta de la derivada
        self.stringderivada = StringVar()
        if self.derivada is not None:
            self.stringderivada.set(self.derivada.AString())
        self.lbDerivada = Label(self.toplevel, textvariable=self.stringderivada, font=letravalores,anchor = "w",
                                background = fondocajas,fg = "Black")
        self.lbDerivada.place(x= 220, y=self.alto/1.18, width = self.ancho/1.85)
        # Añadir la etiqueta del rotulo de los botones de las iteraciones
        self.lbIteraciones = Label(self.toplevel, text="Iteraciones ", font=letrarotulos,
                                      background="seagreen")
        self.lbIteraciones.place(x= self.ancho-self.ancho/3.8, y=self.alto/1.18)
        # Botones de iteraciones
        self.btIzq = Button(background="MidNightBlue", text="\u25C0", font="Arial 29 bold",
                                  command= lambda: self.on_iteraciones_clicked(0), fg="Cyan")
        self.btIzq.place(x=(self.ancho - self.ancho / 3.8) , y=self.alto/1.18+30,
                               height=ladobotones, width=ladobotones)
        self.btDer = Button(background="MidNightBlue", text="\u25B6", font="Arial 29 bold",
                            command= lambda: self.on_iteraciones_clicked(1), fg="Cyan")
        self.btDer.place(x=(self.ancho - self.ancho / 3.8)+ladobotones*1.68 , y=self.alto/1.18+30,
                         height=ladobotones, width=ladobotones)
        # Boton para la resolucion directa
        self.btDirecta = Button(background="Gold", text="Resolver", font="Arial 13 bold",
                                     command=self.on_btDirecta_clicked,fg = "Black")
        self.btDirecta.place(x=(self.ancho - self.ancho / 12) , y=self.alto /1.18+30,
                                  height=ladobotones)
        # Etiqueta del rotulo de las raices
        self.lbRaices = Label(self.toplevel, text="Raices: ", font=letrarotulos,
                                      background="seagreen")
        self.lbRaices.place(x=5, y=self.alto/1.12)
        # Caja de texto de las raices
        self.stringRaices = StringVar()
        if len(self.raices) > 0:
            self.stringRaices.set(str(self.raices[0]))
        self.tbRaices = Entry(self.toplevel, textvariable=self.stringRaices, font=letravalores , background = fondocajas,
                             fg = "Black")
        self.tbRaices.place(x=220, y=self.alto/1.12, width = self.ancho/1.85,height = 30)
        # Boton para la siguiente raiz
        self.btSiguiente = Button(background="LawnGreen", text="Siguiente Raiz", font="Arial 13 bold",
                                command=self.on_btSiguiente_clicked,wraplength = ladobotones*2+5,fg = "Black")
        # Obtener la derivada del polinomio
        self.ObtenerDerivada()


    def Redibujar(self):
        # print(str(self.pd))
        self.daGrafico.delete("all")
        self.PintarEjes()
        if self.factorx > 9:
            self.PintarDivisiones()
        self.PintarGrafico()

    def on_btZoomMas_clicked(self):
        """ Manejador de los botones para el zoom + """

        self.factorx += 10
        self.factory = self.altografico * self.factorx / self.anchografico
        if self.tangente:
            self.pd = True
        self.Redibujar()

    def on_btZoomMenos_clicked(self):
        """ Manejador de los botones para el zoom - """
        if self.factorx > 10:
            self.factorx -= 10
        elif self.factorx > 1:
            self.factorx -= 1
        self.factory = self.altografico * self.factorx / self.anchografico
        if self.tangente:
            self.pd = True
        self.Redibujar()

    def on_btArriba_clicked(self):
        self.desply += 10
        if self.puntero >= 0 and self.tipo == "Newton":
            self.pd = True
        self.Redibujar()

    def on_btAbajo_clicked(self):
        self.desply -= 10
        if self.puntero >= 0 and self.tipo == "Newton":
            self.pd = True
        self.Redibujar()
    def on_btIzquierda_clicked(self):
        self.desplx -= 10
        if self.puntero >= 0 and self.tipo == "Newton":
            self.pd = True
        self.Redibujar()
    def on_btDerecha_clicked(self):
        self.desplx += 10
        if self.puntero >= 0 and self.tipo == "Newton":
            self.pd = True
        self.Redibujar()
    def on_btHome_clicked(self):
        self.desplx = 0
        self.desply = 0
        self.factorx = 60
        self.factory = self.factorx * self.altografico / self.anchografico
        if self.puntero >= 0 and self.tipo == "Newton":
            self.pd = True
        self.Redibujar()

    def on_btDirecta_clicked(self):
        self.directa = True
        while self.pol is not None:
            if self.pol.SinVariables():
                break
            self.ObtenerDerivada()
            nuevaraiz = self.pol.ObtenerRaiz(self.valoresx, self.valoresm, self.precision)
            if nuevaraiz != None:
                self.raices.append(nuevaraiz)
                self.MostrarRaiz()
                raiz = self.raices[len(self.raices) - 1]
                self.pol = self.pol.Ruffini(raiz, self.precision)
                if self.pol is None:
                    break
                self.valoresx.clear()
                self.valoresm.clear()
            else:
                break
        self.Finalizar()

    def on_iteraciones_clicked(self,boton):
        # Ocultar el boton de resolución directa
        self.btDirecta.place_forget()
        if self.puntero > len(self.valoresx):
            return
        self.pd = True
        self.tangente = True
        self.pd = True  # Hace que se pinte la linea tangente
        if boton == 1: # Si se pulsa el boton de iteración adelante
            self.puntero += 1
        else:
            self.puntero -= 1
        # Añadir la leyenda
        if self.puntero == 0:
            entrada = "Comenzamos asignando a la x un valor arbitrario, en este caso 1. ( puntos de color naranja )"
        else:
            entrada = "Asignamos a la x el valor del punto de intersección de la tangente con el eje de" \
                          " abcisas anterior ( puntos de color naranja )."
        self.Mensaje.set(
            entrada + " Aplicando el valor de x a la derivada, obtenemos la tangente ( línea azul ). El punto de"
                        " intersección de la tangente con el eje de abcisas nos proporciona el siguiente valor de x. ( punto verde )")
        self.Redibujar()
        # Si se ha llegado a la última iteración, mostrar la raiz
        if self.puntero == len(self.valoresx):
            if boton == 1:
                self.puntero += 1
            self.MostrarRaiz()
            # Obtener el polinomio reducido con la raiz obtenida
            raiz = self.raices[len(self.raices) - 1]
            reducido = self.pol.Ruffini(raiz,self.precision)


    def MostrarRaiz(self):

        """ Muestra la última raiz del polinomio obtenida """

        self.Mensaje.set("Se ha obtenido una raiz del polinomio.\nPulsar el botón 'Siguiente raiz' para obtener"
                                 " otra raiz del polinomio en caso de que exista.")
        # Mostrar el boton siguiente y ocultar los de iteracion
        self.btSiguiente.place(x=(self.ancho - self.ancho / 5.4), y=self.alto/1.2+30,
                               width = self.ancho / 15, height=40)
        self.btDer.place_forget()
        self.btIzq.place_forget()
        self.lbIteraciones.place_forget()

        historic = self.tbRaices.get()
        raiz = self.raices[len(self.raices) - 1]
        # Obtener la cantidad de decimales que se veran en funcion de la precision
        decimales = ceil(log(1 / self.precision, 10))
        separador = ';' if len(self.raices) > 1 else ''
        self.stringRaices.set(historic + separador + ' x =' + str(round(raiz, decimales)))


    def on_btSiguiente_clicked(self):

        """ Reduce el polinomio con la ultima raiz encontrada, llama a on_draw para dibujarlo y a
            ObtenerDerivada para mostrar su derivada. """
        # Ocultar el boton siguiente y mostrar los de iteracion
        self.btSiguiente.place_forget()
        self.lbIteraciones.place(x= self.ancho-self.ancho/3.8, y=self.alto/1.18)
        self.btIzq.place(x=(self.ancho - self.ancho / 3.8) , y=self.alto/1.18+30,
                               height=40, width=40)
        self.btDer.place(x=(self.ancho - self.ancho / 4.5) , y=self.alto/1.18+30,
                         height=40, width=40)
        # Reducir el polinomio con la ultima raiz obtenida
        raiz = self.raices[len(self.raices) - 1]
        self.pol = self.pol.Ruffini(raiz, self.precision)
        # Resetear las listas
        self.valoresx.clear()
        self.valoresm.clear()
        self.puntero = -1
        if self.tipo == "Biseccion":
            self.puntero = -2
        # Si el polinomio reducido no existe, o es un número finalizar la resolución
        if self.pol == None or self.pol.SinVariables():
            self.Finalizar()
            return
        # Mostrar el polinomio reducido
        self.stringpol.set(self.pol.AString())
        self.Mensaje.set(" Reducimos el polinomio con la última raiz obtenida usando el método de Ruffini.")
        # Dibujar el polinomio reducido
        self.Redibujar()
        # Mostrar la derivada del polinomio reducido
        if self.tipo == "Newton":
            self.derivada = self.ObtenerDerivada()
            if self.derivada is not None:
                self.stringderivada.set(self.Derivada.AString())
        elif self.tipo == "Biseccion":
            self.ObtenerDerivada()


    def ObtenerDerivada(self):

        """ Obtiene la derivada del polinomio y la muestra en la etiqueta de la derivada """
        self.derivada = self.pol.Derivada()
        self.stringderivada.set(self.derivada.AString())
        # Llenar el diccionario de valores X Y y la lista de pendientes
        raiz = self.pol.ObtenerRaiz(self.valoresx, self.valoresm, self.precision)
        self.raices.append(raiz)
        # print("Linea 371 Raiz en GUI: ", raiz)
        # input()
        if raiz is None:
            self.Mensaje.set("El polinomio no tienen raices")
            self.Finalizar()
        else:
            if len(self.raices) == 1:
                self.Mensaje.set( "Mostrando la gráfica del polinomio. Pulsar los botones de iteración adelante o "
                                     "atrás para ver la resolución paso a paso por el método de Newton "
                                     ", o el botón \"Resolver\" para"
                                     " obtener las raices del polinomio de manera directa.")
            else:
                self.Mensaje.set( "Reducimos el polinomio con la última raiz encontrada por el método de Ruffini.\n"
                                  "Mostrando la gráfica del polinomio reducido. Pulsar los botones de iteración adelante "
                                  "o atrás para ver la resolución paso a paso.")
        """
        if raiz is not None and abs(raiz) > self.precision:
            self.Mensaje.set(
                "Pulsar el botón de iteraciones adelante para ver el resultado de la siguiente iteración del método de Newton, o el botón \"Resolver\" para obtener directamente las raices del polinomio introducido. ")
        """

    def PintarEjes(self):
        """ Dibuja los ejes de coordenadas del gráfico """

        # Pintar los ejes
        gruesolinea = 2.5
        # Pintar el eje de abcisas ( X )
        ejeh = self.daGrafico.create_line( 0,self.altografico/2+self.desply,self.anchografico,
                                           self.altografico/2+self.desply,fill = "white",width = gruesolinea)
        # Pintar el eje de ordenadas ( Y )
        ejev = self.daGrafico.create_line(self.anchografico/2+self.desplx,
                                          0,self.anchografico/2+self.desplx,
                                          self.altografico,width = gruesolinea,fill = "white")

        # self.daGrafico.scale(True,0,0,20,20) Ejemplo de escalado, el primer argumento hace que se active o no el escalado, los dos siguientes son el desplazamiento, y los dos ultimos la escala en x e y

    def PintarDivisiones(self):

        # Dibuja las lineas de division en el grafico 
        # Pintar las lineas verticales del lado derecho
        colorlinea = "gray"
        estilolinea = (1,5)
        x = self.anchografico /2+self.desplx
        while x < self.anchografico:
            division = self.daGrafico.create_line(x,0,x,self.altografico,width = 0.1, fill = colorlinea,dash = estilolinea)
            x += 1 * self.factorx
        # Pintar las lineas verticales del lado izquierdo
        x = self.anchografico/2+self.desplx
        while x > 0:
            division = self.daGrafico.create_line(x, 0, x, self.altografico, width=0.1, fill=colorlinea, dash=estilolinea)
            x -= 1 * self.factorx
        # Pintar las lineas horizontales de la parte superior
        y = self.altografico/2+self.desply
        while y > 0 :
            division = self.daGrafico.create_line(0, y, self.anchografico, y, width=0.1, fill= colorlinea, dash=estilolinea)
            y -= 1 * self.factory
        # Pintar las lineas horizontales de la parte inferior
        y = self.altografico/2+self.desply
        while y < self.altografico:
            division = self.daGrafico.create_line(0, y, self.anchografico, y, width=0.1, fill=colorlinea, dash=estilolinea)
            y += 1 * self.factory


    def PintarGrafico(self):
        # Dibuja la curva del polinomio
        x = 0
        colorlinea = "red"
        gruesolinea = 1.5
        limit = 10000
        cyant = self.altografico/2
        cxant = self.anchografico/2
        while x < self.anchografico + self.desplx:
            y = self.pol.EvaluarPolinomio(x)
            # coordenada y en pantalla
            cy = (self.altografico/2 - y*self.factory + self.desply)
            # coordenada x en pantalla
            cx = self.anchografico/2 + x*self.factorx + self.desplx
            if cy  < -limit or cy > self.altografico+limit:
                break
            # print(" X " + str(x) + "  y  " + str(y) + "  cx " + str(cx) + "  cy  " + str(cy))
            # input()
            if x > 0:
                linea = self.daGrafico.create_line(cxant, cyant, cx, cy, width=gruesolinea, fill=colorlinea)
            x += GUI.incremento
            cyant = cy
            cxant = cx
        x = 0
        while x < self.anchografico + self.desplx:
            y = self.pol.EvaluarPolinomio(-x)
            cy = (self.altografico / 2 - y * self.factory + self.desply)
            cx = self.anchografico / 2 - x * self.factorx + self.desplx
            if cy < -limit or cy > self.altografico+limit:
                break
            # print(" X " + str(x) + "  y  " + str(y) + "  cx " + str(cx) + "  cy  " + str(cy))
            # input()
            if x > 0:
                linea = self.daGrafico.create_line(cxant, cyant, cx, cy, width=gruesolinea, fill=colorlinea)
            x += GUI.incremento
            cyant = cy
            cxant = cx
        if self.pd:
            if self.tipo == "Newton":
                self.PintarDerivada()

    def PintarDerivada(self):
        """ Dibuja la linea tangente a la gráfica del polinomio en la posicion X que marca el puntero
            muestra los valores de X , Y , m  y X siguiente y las indicaciones en cada iteracion """

        if self.puntero < 0:
            self.puntero = 0
        self.pd = False
        # Obtener una lista con los valores de las claves en el diccionario
        claves = list(self.valoresx.keys())
        # Obtener el factor de escala para los valores de Y
        factory = self.altografico / self.anchografico * self.factorx
        # Obtener las coordenadas del punto de interseccion de la tangente
        if self.puntero < len(self.valoresx):
            x = claves[self.puntero]
            y = self.valoresx[x]
            # Obtener las coordenadas del punto de interseccion  de la tangente con el graficoen pantalla
            xp = self.anchografico/2+self.desplx + x * self.factorx
            yp = self.altografico/2+self.desply - y * factory
            # Pintar el punto de interseccion  de la tangente con la grafica
            self.daGrafico.create_oval(xp - 5 , yp - 5, xp + 5, yp + 5, fill="orange")
            # Pintar la linea de puntos y el punto del valor de x
            self.daGrafico.create_line(xp,yp,xp,self.altografico/2+self.desply,fill = "orange",dash = (1,5),width = 0.1)
            self.daGrafico.create_oval(xp-5,self.altografico/2+self.desply-5,xp+5,self.altografico/2+self.desply+5,fill= "orange")
            # Obtener el vector paralelo a la tangente
            xp2 = xp+1000*self.factorx
            yp2 = yp - self.valoresm[self.puntero] * 1000 * factory
            xp3 = xp-1000*self.factorx
            yp3 = yp + self.valoresm[self.puntero] * 1000 * factory
            # Pintar la tangente
            self.daGrafico.create_line(xp2,yp2,xp3,yp3,fill="Cyan")
            # Obtener el punto de interseccion de la tangente con el eje de abcisas
            intersecc = self.anchografico/2+(-y/self.valoresm[self.puntero] + x)*self.factorx
            # Pintar el punto de interseccion de la tangente con el eje de abcisas
            self.daGrafico.create_oval(intersecc-5+self.desplx,self.altografico/2+self.desply-5,intersecc+5+self.desplx,self.altografico/2+self.desply+5,fill = "chartreuse")
            """
            if self.puntero < len(claves):
                # Pintar el siguiente punto de interseccion de la tangente
                puntosiguiente = self.altografico/2 - self.valoresx[claves[self.puntero+1]] * self.factory
                self.daGrafico.create_oval(intersecc-5,puntosiguiente-5,intersecc+5,puntosiguiente+5,fill = "magenta")
                # Pintar la linea de puntos
                self.daGrafico.create_line(intersecc,self.altografico/2-self.desply,intersecc,puntosiguiente,
                                       fill = "magenta",width = 0.1, dash = (1,5))
            """
            # Obtener la cantidad de decimales que se veran en funcion de la precision
            decimales = ceil(log(1 / self.precision, 10))
            # Mostrar los valores en las etiquetas
            self.stringvalorx.set(str(round(x,decimales)))
            valory = str(round(y, decimales))
            self.stringvalory.set(valory)
            self.stringvalorpendiente.set(str(round(self.valoresm[self.puntero], decimales)))
            if self.puntero < len(claves)-1:
                self.stringnuevax.set(str(round(claves[self.puntero+1],decimales)))
            else:
                self.stringnuevax.set(str(round(self.raices[len(self.raices)-1],decimales)))
            # Incrementar el puntero
            # self.puntero += 1


    def Finalizar(self):

        """  Finaliza una vez obtenidas todas las raices del polinomio """
        self.stringpol.set(self.polmemo.AString())
        self.pol = self.polmemo
        self.ObtenerDerivada()
        self.stringderivada.set(self.derivada.AString())
        self.Redibujar()
        self.Mensaje.set("Se han hallado todas las raices del polinomio.")
        # Ocultar los botones
        self.btDer.place_forget()
        self.btIzq.place_forget()
        self.btSiguiente.place_forget()
        self.btDirecta.place_forget()
        self.lbIteraciones.place_forget()
        self.lbVx.place_forget()
        self.lbX.place_forget()
        self.lbVY.place_forget()
        self.lbY.place_forget()
        self.lbVm.place_forget()
        self.lbM.place_forget()
        self.lbNuevax.place_forget()
        self.lbVnuevax.place_forget()



