from tkinter import *
from GUI import GUI

import math


class GUIBiseccion(GUI):
    def __init__(self,pol,precision,root,Mensaje,tbPol,stringpol):
        self.extremos = []  # Para guardar los valores de los extremos
        #self.puntero = 0 # Inicializar el puntero de situacion del elemento de las listas de valores que se mostrará
        GUI.__init__(self,pol,precision,root,Mensaje,tbPol,"Biseccion",stringpol) # Inicializar la clase base
        # Modificar los rotulos de las cajas del lado izquierdo
        self.lbVx.configure(text = "Extremo izquierdo")
        self.lbVx.place(x = (self.ancho - self.ancho/5) + (self.ancho/5 - self.lbVx.winfo_width())/4, y = self.alto/4.2)
        self.lbVY.configure(text = "Extremo derecho")
        self.lbVY.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5 - self.lbVY.winfo_width()) / 4, y=self.alto/3.16)
        self.lbVm.configure(text="Punto medio")
        self.lbVm.place(x=(self.ancho - self.ancho / 5) + (self.ancho / 5 - self.lbVm.winfo_width()) / 3, y=self.alto/2.48)
        self.lbNuevax.place_forget()
        self.lbVnuevax.place_forget()
        self.puntero = -2

    def ObtenerDerivada(self):
        """ Llena la lista de valores de los extremos de los intervalos y de valores del polinomio en
        los puntos medios hasta que el valor del polinomio en el punto medio es menor a la precision """
        # Ocultar la etiqueta del rotulo y la caja de texto de la derivada
        self.lbDerivada.place_forget()
        self.lbRotuloDerivada.place_forget()
        # Llenar la lista de extremos y la lista de valores del polinomio en el punto medio de cada extremo
        raiz = self.pol.ObtenerRaizBiseccion(self.extremos,self.valoresm,self.precision)
        self.raices.append(raiz)
        if raiz is None:
            self.Mensaje.set("El polinomio no tiene raices")
            if len(self.raices) == 0:
                self.lbRaices.place_forget()
                self.tbRaices.place_forget()
            else:
                if not self.directa:
                    self.Finalizar()
            self.lbIteraciones.place_forget()
            self.btSiguiente.place_forget()
            self.btDirecta.place_forget()
        # Dibujar el polinomio
        if raiz is not None and abs(raiz) > self.precision:
            self.Mensaje.set("Pulsar el botón de iteraciones adelante para ver el resultado de la siguiente iteración "
                             " del método de bisección, o el botón \"Resolver\" para obtener directamente las raices del polinomio introducido. ")


    def on_iteraciones_clicked(self, boton):
        """ Dibuja los extremos del intervalo, el punto medio y el valor del polinomio en el punto medio
        en función de la posicion del puntero en las listas de valores del intervalo y de valores del
        punto medio obtenidas en ObtenerDerivada """

        if boton == 1:
            self.Redibujar()
            self.puntero += 2
        else:
            self.Redibujar()
            self.puntero -= 2
        self.PintarIntervalo()


    def PintarIntervalo(self):
        """ Dibuja el intervalo en funcion de la posicion del puntero """

        if self.directa:
            return
        if self.puntero < 0:
            self.puntero = 0
        factory = self.altografico/self.anchografico*self.factorx
        # Obtener los valores de los extremos y el punto medio del intervalo y los valores del polinomio
        # en esos puntos
        try:
            a = self.extremos[self.puntero]
            b = self.extremos[self.puntero + 1]
        except IndexError:
            return
        fa = self.pol.EvaluarPolinomio(a)
        fb = self.pol.EvaluarPolinomio(b)
        medio = (a + b) / 2
        fmedio = self.pol.EvaluarPolinomio(medio)
        # Mostrar los valores en las etiquetas del lado izquierdo
        valorizquierdo = "x: " + str(round(a,3)) + "     fx: " + str(round(fa,3))
        valorderecho = "x: " + str(round(b,3)) + "     fx: " + str(round(fb,3))
        puntomedio = "x: " + str(round(medio,3)) + "     fx: " + str(round(fmedio,3))
        self.stringvalorx.set(valorizquierdo)
        self.stringvalory.set(valorderecho)
        self.stringvalorpendiente.set(puntomedio)

        if self.puntero == 0:
            lectura = "Comenzamos estableciendo un intervalo entre dos puntos, punto de color azul claro y " \
                      "de color azul teniendo en cuenta que" \
                      " el valor del polinomio en los extremos del intervalo tenga signo distinto." \
                      "Así aseguramos la existencia de un punto dentro del intervalo donde el valor" \
                      " del polinomio sea cero.\nEn cada iteración se calcula el punto medio entre los extremos" \
                      ", punto verde, y el valor del polinomio en ese punto. Cuando el valor del " \
                      "polinomio en el punto medio sea menor que la precisión introducida, se habrá obtenido una " \
                      "raiz del polinomio."
        else:
            lectura =""
        if self.puntero == len(self.extremos) - 2:
            self.raices.append(medio)
            self.MostrarRaiz()
            self.extremos.clear()
            return
        elif fa * fmedio < 0:
            lectura += "\nComo el valor del polinomio en el extremo izquierdo multiplicado por el valor" \
                        "del polinomio en el punto medio es negativo, el extremo izquierdo se mantendrá" \
                        " en la siguiente iteración. El punto medio pasará a ser el extremo derecho."
        elif fb * fmedio < 0:
            lectura += "\nComo el valor del polinomio en el extremo derecho multiplicado por el valor " \
                        "del polinomio en el punto medio es negativo, el extremo derecho se mantendrá " \
                        "en la siguiente iteración. El punto medio pasará a ser el extremo izquierdo."
        # Dibujar el extremo izquierdo
        self.daGrafico.create_oval(self.anchografico/2 + a*self.factorx + self.desplx-5,
                                   self.altografico/2 + self.desply-5,
                                   self.anchografico/2 + a*self.factorx + self.desplx+5,
                                   self.altografico/2 + self.desply+5,fill = "Coral")
        # Dibujar el valor del polinomio en el extremo izquierdo
        self.daGrafico.create_oval(self.anchografico/2+a*self.factorx+self.desplx-5,self.altografico/2-fa*factory+self.desply-5,
                                   self.anchografico/2+a*self.factorx+self.desplx+5,self.altografico/2-fa*factory+self.desply+5,
                                   fill = "Coral")
        # Dibujar la linea entre el extremo izquierdo y el valor del polinomio en ese punto
        self.daGrafico.create_line(self.anchografico/2+a*self.factorx+self.desplx, self.altografico/2+self.desply,
                                   self.anchografico/2+a*self.factorx+self.desplx,self.altografico/2-fa*factory+self.desply,
                                   fill = "Coral")
        # Dibujar el extremo derecho
        self.daGrafico.create_oval(self.anchografico/2+b*self.factorx+self.desplx-5,self.altografico/2+self.desply-5,
                                   self.anchografico/2+b*self.factorx+self.desplx+5,self.altografico/2+self.desply+5,
                                   fill = "Cyan")
        # Dibujar el valor del polinomio en el extremo derecho
        self.daGrafico.create_oval(self.anchografico / 2 + b * self.factorx + self.desplx - 5,
                                   self.altografico / 2 - fb*factory + self.desply - 5,
                                   self.anchografico / 2 + b * self.factorx + self.desplx + 5,
                                   self.altografico / 2 - fb*factory + self.desply + 5,
                                   fill="Cyan")
        # Dibujar la linea entre el extremo derecho y el valor del polinomio en ese punto
        self.daGrafico.create_line(self.anchografico / 2 + b * self.factorx + self.desplx,
                                   self.altografico / 2 + self.desply,
                                   self.anchografico / 2 + b * self.factorx + self.desplx,
                                   self.altografico / 2 - fb * factory + self.desply,
                                   fill="Cyan")
        # Dibujar el punto medio del intervalo
        self.daGrafico.create_oval(self.anchografico/2+medio*self.factorx+self.desplx-5,self.altografico/2+self.desply-5,
                                   self.anchografico/2+medio*self.factorx+self.desplx+5,self.altografico/2+self.desply+5,
                                   fill = "Chartreuse")
        # Dibujar valor del polinomio en el punto medio del intervalo
        self.daGrafico.create_oval(self.anchografico / 2 + medio * self.factorx + self.desplx - 5,
                                   self.altografico / 2 - fmedio*self.factory + self.desply - 5,
                                   self.anchografico / 2 + medio * self.factorx + self.desplx + 5,
                                   self.altografico / 2- fmedio*factory + self.desply + 5,
                                   fill="Chartreuse")
        # Dibujar la linea entre el punto medio y el valor del polinomio en ese punto
        self.daGrafico.create_line(self.anchografico / 2 + medio * self.factorx + self.desplx,
                                   self.altografico / 2 + self.desply,
                                   self.anchografico / 2 + medio * self.factorx + self.desplx,
                                   self.altografico / 2 - fmedio * factory + self.desply,
                                   fill="Chartreuse")
        # Mostrar la lectura en la etiqueta de los mensajes
        self.Mensaje.set(lectura)

    """ Reddefinicion de los metodos de Zoom y Desplazamiento """

    def on_btZoomMas_clicked(self):
        GUI.on_btZoomMas_clicked(self)
        self.PintarIntervalo()

    def on_btZoomMenos_clicked(self):
        GUI.on_btZoomMenos_clicked(self)
        self.PintarIntervalo()

    def on_btAbajo_clicked(self):
        GUI.on_btAbajo_clicked(self)
        self.PintarIntervalo()

    def on_btArriba_clicked(self):
        GUI.on_btArriba_clicked(self)
        self.PintarIntervalo()

    def on_btDerecha_clicked(self):
        GUI.on_btDerecha_clicked(self)
        self.PintarIntervalo()

    def on_btIzquierda_clicked(self):
        GUI.on_btIzquierda_clicked(self)
        self.PintarIntervalo()

    def on_btHome_clicked(self):
        GUI.on_btHome_clicked(self)
        self.PintarIntervalo()