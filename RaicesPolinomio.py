
from Polinomio import StringAPolinomio
from Polinomio import PolinomioCopia

import GUI
from GUI import GUI

import GUIBiseccion
from GUIBiseccion import GUIBiseccion

import GUIRegula
from GUIRegula import Regula

from tkinter import *
from tkinter import ttk

import sys
import os


class RaicesPolinomio():
    def __init__(self):
        # Inicializar la ventana principal
        self.root = Tk()
        self.root.title("RaicesPolinomio Xadnem 20")
        self.root.configure(background="seagreen") # Cambiar el color del fondo
        self.alto,self.ancho = self.root.winfo_screenheight(), self.root.winfo_screenwidth() # Obtener el ancho y alto de la pantalla
        self.root.geometry("%dx%d+0+0" % (self.ancho, self.alto)) # Cambiar el tamaño de la ventana

        # Etiqueta de los mensajes
        self.Mensaje = StringVar()
        self.Mensaje.set("Introducir el polinomio.")
        self.lbMensajes = Label(self.root, textvariable=self.Mensaje,bg="darkseagreen", fg = "Black",
                                justify="left", anchor=W,font=("Dejavu", 17),wraplength = self.ancho-150,pady = "0")
        self.lbMensajes.place(x = 5, y = 0, height = self.alto/7, width = self.ancho-150)

        # Botones de navegacion
        self.btSalir=Button(self.root,text="Salir",background = "red",
                            command = self.on_btSalir_clicked, fg = "orange", activebackground = "Yellow")
        self.btSalir.place(x = self.ancho-120,y = 10,width = 70, height = 30)

        self.btNuevo=Button(self.root,text="Nuevo",background = "silver",activebackground = "Yellow",fg = "Black",
                            command = self.on_btNuevo_clicked)
        self.btNuevo.place(x = self.ancho-120, y = 60,width = 70,height = 30)

        # Rotulos del polinomio y la precision
        self.lbPol = Label(self.root, text="Polinomio", background="seagreen",
                           font=("Dejavu", 17, "underline", "italic"), width=75)
        self.lbPol.place(x = 0, y = self.alto/7+5,height = 30,width = self.ancho-(self.ancho/5))

        self.lbPrecision = Label(self.root, text="Precisión", background="seagreen",
                           font=("Dejavu", 17, "underline", "italic"))

        # Cajas del polinomio y la precision
        self.stringpol = StringVar()
        self.tbPol = Entry(self.root,font = "Dejavu 17 bold",textvariable = self.stringpol,background = "lightgreen",
                           fg = "Black")
        self.tbPol.place(x = 5, y = self.alto/7+35,height = 30, width = self.ancho-(self.ancho/5))
        self.tbPol.focus()
        self.tbPol.bind('<Return>', self.on_tbPol_activate)
        self.tbPrecision = Entry(self.root,font="Dejavu 17 bold",background = "lightgreen",fg = "Black")
        self.tbPrecision.bind('<Return>',self.on_tbPrecision_activate)
        
        # RadioButtons para las opciones de resolución
        # RadioButton que no se verá para iniciar el grupo
        self.resolucion = StringVar()
        self.resolucion.set(None)
        self.rbNewton = Radiobutton(text = "Método de Newton",background = "seagreen",font = "Dejavu 17",
                                    variable = self.resolucion,value = "Newton",command = self.on_RadioButtons_clicked,
                                    relief = "raised")

        self.rbBiseccion = Radiobutton(text = "Método de Bisección",background = "seagreen",font = "Dejavu 17",
                                       variable = self.resolucion,value = "Biseccion",
                                       command = self.on_RadioButtons_clicked,relief = "raised")
        self.rbFalsi = Radiobutton(text="Método Regula Falsi", background="seagreen", font="Dejavu 17",
                                       variable=self.resolucion, value="Falsi",
                                       command=self.on_RadioButtons_clicked, relief = "raised")
        self.root.mainloop()


    def on_btSalir_clicked(self):
        """ Manejador para el boton Salir """

        sys.exit()


    def on_btNuevo_clicked(self):

        """ Manejador para el boton Nuevo, que reinicia el programa """

        python = sys.executable
        os.execl(python, python, *sys.argv)

    def on_tbPol_activate(self,widget):

        """ Manejador del evento activate de la caja de texto para introducir el polinomio.
            Construye un objeto Polinomio con el contenido de la caja de texto y lo asigna
            al atributo pol de la clase."""
        ps = self.tbPol.get()
        if len(ps) == 0:
            return
        self.pol = StringAPolinomio(ps)
        if self.pol is None:
            #dialogo = Gtk.MessageDialog(self,0,Gtk.MessageType.INFO,Gtk.ButtonsType.OK,"El polinomio introducido está"
            #                                                                           " mal formado. Ejemplo de formato:"
            #                                                                           " 3x^3+2X-8")
           # dialogo.run()
            entrada = self.tbPol.get()
            self.tbPollectura.set( "Entrada no válida:  "+ entrada)
            self.tbPol.focus()
            self.tbPol.select_range(0,"end")
        else:
            #self.polmemo = PolinomioCopia(self.pol)
            self.ObtenerPrecision()

    def ObtenerPrecision(self):

        """Muestra la caja de texto para introducir la precisión con la que se realizará el
           cálculo """
        # Mostrar el rotulo y la caja de texto para la precision y poner el foco en la misma
        self.lbPrecision.place(x = self.ancho-(self.ancho/5),y = self.alto/7+5,height = 30, width = (self.ancho/5))
        self.tbPrecision.place(x = self.ancho-(self.ancho/5)+15,y = self.alto/7+35,height = 30, width = (self.ancho/5)-40)
        self.Mensaje.set("Introducir la precisión de calculo.")
        self.tbPrecision.focus()

    def on_tbPrecision_activate(self, widget):

        """ Si el valor es correcto, asigna el valor introducido en la caja de texto al atributo precision"""
        precisions = self.tbPrecision.get()
        precisions.replace(",", ".")
        try:
            self.precision = float(precisions)
            if self.precision >= 1 or self.precision < 0:
                raise ValueError
            # Mostrar los radiobuttons con los posibles métodos de resolucion
            self.MostrarOpciones()
            #self.hbGraficoValores.set_visible(True)
            # Mostrar la derivada del polinomio
            # self.ObtenerDerivada()
        except ValueError:
            self.tbPrecision.focus()
            self.tbPrecision.select_range(0,'end')
            self.Mensaje.set("Introducir valores entre 0 y 1 ( Ejemplo: 0.001)")

    def MostrarOpciones(self):
        """ Muestra los radiobuttons con los posibles métodos de resolución"""

        self.Mensaje.set("Seleccionar un método de resolución.")
        situaciony = self.alto/4
        self.rbNewton.place(x = 10, y = situaciony)
        self.rbBiseccion.place(x = 10, y = situaciony+50)
        self.rbFalsi.place(x=10, y=situaciony+100)
        self.lbMensajes.focus()

    def on_RadioButtons_clicked(self):
        """ Inicia la resolución dependiendo del radiobutton que se haya activado """
        # Eliminar los radiobuttons
        self.rbNewton.destroy()
        self.rbBiseccion.destroy()
        self.rbFalsi.destroy()
        metodo = self.resolucion.get()
        if metodo == "Newton":
            self.tipo = "Newton"
            self.Mensaje.set("Método de Newton")
            resolucionNewton = GUI(self.pol,self.precision,self.root,self.Mensaje,self.tbPol,self.tipo,self.stringpol)
        elif metodo == "Biseccion":
            self.tipo = "Biseccion"
            self.Mensaje.set("Método de Bisección")
            resolucionBiseccion = GUIBiseccion(self.pol,self.precision,self.root,self.Mensaje,self.tbPol,self.stringpol)
        elif metodo == "Falsi":
            self.tipo = "Regula"
            self.Mensaje.set("Metodo de Regula Falsi")
            resolucionFalsi = Regula(self.pol,self.precision,self.root,self.Mensaje,self.tbPol,self.stringpol)
            self.lbMensajes.configure(font=("Dejavu",14,"bold"))



Raicespolinomio = RaicesPolinomio()

